# 项目结构

-- app: app为主moudle，分xiaomi，huawei渠道；

-- channelhuawei:华为渠道，提供华为接口

-- channelxiaomi：小米渠道，提供小米接口

# 项目描述

 app分xiaomi，huawei渠道，在xiaomi渠道引用 channelhuawei ；在 huawei 渠道引用 channelxiaomi 。

 ChannelType 类用于抽象各个渠道的差异，对外提供统一的接口。

 如华为提供了 ProductFuns.getProductName()获取厂商名称 ，小米提供了 DevicesFuns.getDevicesName()获取设备名称；

 而ChannelType用去去除 华为和小米的差异，对外统一提供getChannelType方法。

 # 项目配置
 * 1、设置渠道维度，这里只设置了一个维度。“channel”为维度名称

  flavorDimensions("channel")

 * 2、设置渠道，这里只设置了两个渠道。

 productFlavors {
         xiaomi { dimension "channel" }
         huawei {dimension "channel"}
     }

 * 3、configurations设置按渠道引用。
 
 configurations {
     xiaomiImplementation
     huaweiImplementation
 }

  xiaomiImplementation project(':channelxiaomi')
  huaweiImplementation project(':channelhuawei')